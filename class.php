<?php
/**
 * The interface provides the contract for different readers
 * E.g. it can be XML/JSON Remote Endpoint, or CSV/JSON/XML local files
 */
interface ReaderInterface {
 /**
 * Read in incoming data and parse to objects
 */
    public function read(string $input): ?OfferCollectionInterface;
}
/**
 * Interface of Data Transfer Object, that represents external JSON data
 */
interface OfferInterface {
    //mine : maker interface
    
}
/**
 * Interface for The Collection class that contains Offers
 */
interface OfferCollectionInterface {
    public function get(int $index): OfferInterface;
    public function getIterator(): Iterator;
}

class Offer implements OfferInterface {
    public int $offerId;
    public string $productTitle; 
    public int $vendorId;
    public float $price;
}


class OfferCollection implements OfferCollectionInterface{

    private array $offersArray;

    public function __construct($input){

        foreach($input as $key =>$value){
            $offer = new Offer();
            $offer->offerId = $value->offerId;
            $offer->productTitle = $value->productTitle;
            $offer->vendorId = $value->vendorId;
            $offer->price = $value->price;
            $this->offersArray[] = $offer;

        }
    }
    public function get(int $index): OfferInterface{
        return $this->offersArray[$index];
    }
    public function getIterator(): Iterator{
        return new ArrayIterator($this->offersArray);
    }
}

class JsonReader implements ReaderInterface{
    public function read(string $input): ?OfferCollectionInterface{
        if($input){
            $data = json_decode(file_get_contents($input));
            if($data){
                $offerCollection = new OfferCollection($data);
                return $offerCollection;
            }
        }
    }
}

class Logger{
    private static $filename = "logs.txt";

    public static function info($message): void {
        self::log($message, "INFO");
    }
    public static function error($message): void {
        self::log($message, "ERROR");
    }
    private function log($message, $type): void {
        $myfile = fopen(self::$filename, "a") or die("Problem in Opening File!");
        $txt = "[$type] $message    \n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }
}