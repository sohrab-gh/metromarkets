<?php
require_once('class.php');

$json_url = 'data.json'; 
$jsonReadr = new JsonReader();
$offers = $jsonReadr->read($json_url);



$count_by_price_range = function ($from, $to) use ($offers){

    Logger::info("calling parameters are $from and $to in count_by_price_range  ");
    $count = 0;
    foreach ($offers->getIterator() as $offer) {
        if ($offer->price >= $from && $offer->price <= $to) {
            $count++;
        }
    }
    return $count;
};
$count_by_vendor_id = function($id) use ($offers){

    Logger::info("calling parameters is $id in count_by_vendor_id  ");
    $count = 0;
    foreach($offers->getIterator() as $offer){
        if($offer->vendorId == $id){
            $count++;
        }
    }
    return $count;
};






